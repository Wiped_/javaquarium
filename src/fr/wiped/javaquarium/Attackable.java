package fr.wiped.javaquarium;

/**
 * Created by Sacha on 01/07/2018 for Javaquarium.
 */
public interface Attackable extends Killable {

    /**
     * @param eater Poisson qui à manger le poisson
     */
    void attacked(Fish eater);
}
