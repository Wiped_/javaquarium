package fr.wiped.javaquarium;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static int round = 1;

    public static void main(String[] args) {

        //DECLARATION DES VARIABLES
        Random r1 = new Random();
        Scanner sc = new Scanner(System.in);
        int numberFishs;
        int numberSeaweed;
        Aquarium aquarium = new Aquarium();
        char verification = 'O';

        //AFFECTATION DES TYPE DE NOURRITURE AUX RACES
        Fish.initRace();

        //SI LE TOUR EST EGAL A 1 ALORS ON DÉFINIT LES POISSONS
        if (round == 1) {
            System.out.println("Ceci est le tour 0, définissez le nombre de poissons");

            numberFishs = sc.nextInt();

            sc.nextLine();

            //DEFINITION DES POISSONS
            for (int i = 0; i < numberFishs; i++) {
                System.out.println("Il vous reste " + (numberFishs - i) + " Poissons à définir");
                System.out.println("<name> <race>");
                int randomsex = r1.nextInt(Gender.values().length);
                String[] mot = sc.nextLine().split(" ");
                Fish f = new Fish(mot[0], null, setRaceByString(mot[1]), null ,0, 10, aquarium);

                if(f.getRace().equals(Race.CARPE) || f .getRace().equals(Race.THON)){
                    f.setType(Sexuality.MONO_SEXUED);
                }
                if(f.getRace().equals(Race.BAR) || f.getRace().equals(Race.MEROU)){
                    f.setType(Sexuality.HERMAPHRODITE_AGE);
                }
                if(f.getRace().equals(Race.SOLE) || f.getRace().equals(Race.POISSON_CLOWN)){
                    f.setType(Sexuality.HERMAHRODITE_OPPORTUNISTE);
                }

                if(f.getType().equals(Sexuality.MONO_SEXUED)){
                    f.setGender(Gender.values()[randomsex]);
                }else if(f.getType().equals(Sexuality.HERMAPHRODITE_AGE)){
                    f.setGender(Gender.MALE);
                }
                aquarium.addFish(f);
            }

            //DEFINITION DES ALGUES
            System.out.println("Définissez maintenant le nombre d'Algues");

            numberSeaweed = sc.nextInt();
            sc.nextLine();

            for (int i = 0; i < numberSeaweed; i++) {

                Seaweed s = new Seaweed(0, 10, aquarium);
                aquarium.addSeaweed(s);
            }

            //ANNONCE DES OBJETS DE DEPART
            System.out.println("Vous commencez avec " + numberFishs + " poissons voici les information : ");
            for (Fish p : aquarium.getFishInAquarium()) {
                System.out.println(p.toString());
            }
            System.out.println("Et " + numberSeaweed + " Algues");
            System.out.println("Voulez vous passer à la manche suivante ? (O/N)");
            verification = sc.nextLine().charAt(0);
            round++;
        }

        //SYSTEME DE MANCHES
        while (round > 1 && aquarium.getFishInAquarium().size() != 0 && verification == 'O' && aquarium.getSeaweedInAquarium().size() != 0) {
            System.out.println("------- MANCHE: " + round + "--------");

            /*
            Système de reproduction
            Système de Vie
            Système d'Age
             */
            for (Seaweed seaweed : aquarium.getSeaweedInAquarium()) {
                seaweed.reproduce();
                seaweed.setPV(seaweed.getPV() + 1);
                seaweed.setAge(seaweed.getAge() + 1);
                if (seaweed.getAge() > 20) {
                    seaweed.kill();
                }
                if (seaweed.getPV() == 0) {
                    seaweed.kill();
                }



            }

            for (Fish fish : aquarium.getFishInAquarium()) {
                fish.reproduce();
                fish.setPV(fish.getPV() - 1);
                fish.setAge(fish.getAge() + 1);
                if (fish.getAge() > 20) {
                    fish.kill();
                }
                if (fish.getPV() == 0) {
                    fish.kill();
                }

               if(fish.getAge() == 10 && fish.getType().equals(Sexuality.HERMAPHRODITE_AGE)){
                    fish.setGender(Gender.FEMALE);
               }

            }

            //SYSTEME D'ALIMENTATION


            for (Fish fish : aquarium.getFishInAquarium()) { //LIGNE 87
                int a2 = r1.nextInt(aquarium.getFishInAquarium().size());
                int a3 = r1.nextInt(aquarium.getSeaweedInAquarium().size());

                Fish fish_eated = aquarium.getFishInAquarium().get(a2);
                Seaweed seaweed_eated = aquarium.getSeaweedInAquarium().get(a3);
                if (fish.getPV() <= 5) {
                    if (fish.isherbivorous()) {
                        seaweed_eated.attacked(fish);
                    } else if (fish.isCarnivorous() && fish_eated.getRace() != fish.getRace() && fish_eated != fish) {
                        fish_eated.attacked(fish);
                    }
                }

            }


            //AFFICHAGE DES INFOS
            for (Fish fish : aquarium.getFishInAquarium()) {
                System.out.println(fish.toString());
            }

            System.out.println("Il vous reste " + aquarium.getSeaweedInAquarium().size() + " Algues");

            verification = 'N';
            System.out.println("Voulez vous passer à la manche suivante ? (O/N)");
            verification = sc.nextLine().charAt(0);
            round++;
        }

        System.out.println("---------FIN----------");
        System.exit(0);

    }


    //METHODE DE RECUPERATION DE RACE A PARTIR DE STRINGS

    /**
     * Permet de récupérer une race à partir d'un String
     *
     * @param race String de la race récupérée
     * @return Une race à partir du String
     */
    private static Race setRaceByString(String race) {
        return Arrays.stream(Race.values()).filter(r -> r.getRace().equalsIgnoreCase(race)).findFirst().orElse(null);

    }

}
