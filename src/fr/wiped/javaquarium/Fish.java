package fr.wiped.javaquarium;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Fish extends LivingEntity implements Attackable {

    private static int i = 0;
    private String name;
    private Gender gender;
    private Race race;
    private Sexuality type;
    private Aquarium aquarium;
    private static List<Race> carnivorous = new ArrayList<>();
    private static List<Race> herbivorous = new ArrayList<>();

    public Fish(String name, Gender gender, Race race, Sexuality type, int age, int pv,  Aquarium aquarium) {
        super(age, pv);
        super.ID = i++;
        this.name = name;
        this.race = race;
        this.gender = gender;
        this.type = type;
        this.aquarium = aquarium;
    }


    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {

        return name;
    }

    public Race getRace() {
        return race;
    }

    public boolean isCarnivorous() {

        return carnivorous.contains(race);
    }

    public boolean isherbivorous() {

        return herbivorous.contains(race);
    }



    /**
     * Permet d'afficher les informations sur un poisson
     *
     * @return Le nom + le gender du poisson
     */
    @Override
    public String toString() {
        return  "\nNom : " + getName() + "\nID: " +  getID() + "\n Sexe : " + getGender() + "\nSexualité : " + this.getType() + "\n Race : " + getRace() + "\nAge : " + super.getAge() + "\nPV : " + super.getPV();
    }

    public void attacked(Fish fish) {
        System.out.println("Le poisson " + fish.getName() + " à attaqué le poisson " + this.getName() + "(4 PV)");
        this.setPV(fish.getPV() - 4);

        if (this.getPV() <= 0) {
            this.kill();
            fish.setPV(fish.getPV() + 5);
        }
    }

    public void kill() {
        aquarium.removeFish(this);
        System.out.println("Le poisson " + this.getName() + " est mort");
    }

    @Override
    public void reproduce() {
        if (this.getPV() > 5) {
            Random r = new Random();
            int x1 = r.nextInt(aquarium.getFishInAquarium().size());
            int x2 = r.nextInt(Gender.values().length);
            int x3 = r.nextInt(Sexuality.values().length);

            Fish randomFish = aquarium.getFishInAquarium().get(x1);

            if (!(this.getGender().equals(randomFish.getGender())) && this.getRace().equals(randomFish.getRace()) && !(this.getType().equals(Sexuality.HERMAHRODITE_OPPORTUNISTE))) {
                Fish child = new Fish(this.getName() + "_enfant", Gender.values()[x2], this.getRace(), Sexuality.values()[x3], 0, 10, aquarium);
                aquarium.addFish(child);
                System.out.println("Le poisson " + this.getName() + " a fait un enfant avec " + randomFish.getName());
            }
                if(this.getType().equals(Sexuality.HERMAHRODITE_OPPORTUNISTE)){
                this.setType(randomFish.getType());
                Fish child = new Fish(this.getName() + "_" + super.ID, Gender.values()[x2], this.getRace(), Sexuality.values()[x3], 0, 10, aquarium);
                aquarium.addFish(child);
                System.out.println("Le poisson " + this.getName() + " a fait un enfant avec " + randomFish.getName());
            }
        }

    }

    public Sexuality getType() {
        return type;
    }

    public void setType(Sexuality type) {
        this.type = type;
    }

    /**
     * Initialise les types de race à un type de nourriture
     */
    public static void initRace() {
        carnivorous.add(Race.MEROU);
        carnivorous.add(Race.POISSON_CLOWN);
        carnivorous.add(Race.THON);

        herbivorous.add(Race.BAR);
        herbivorous.add(Race.SOLE);
        herbivorous.add(Race.CARPE);
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}