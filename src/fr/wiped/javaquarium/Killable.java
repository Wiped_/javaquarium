package fr.wiped.javaquarium;

/**
 * Created by Sacha on 01/07/2018 for Javaquarium.
 */
public interface Killable {

    void kill();

}
