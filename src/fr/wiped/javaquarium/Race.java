package fr.wiped.javaquarium;

public enum Race {


    MEROU("Merou"),
    THON("Thon"),
    POISSON_CLOWN("Clown"),
    SOLE("Sole"),
    BAR("Bar"),
    CARPE("Carpe");


    private String race;

    Race(String race) {
        this.race = race;
    }

    public String getRace() {
        return race;
    }


}

