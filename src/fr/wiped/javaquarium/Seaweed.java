package fr.wiped.javaquarium;

public class Seaweed extends LivingEntity implements Attackable {

    private static int i;
    private Aquarium aquarium;

    /**
     * Constructeur
     *
     */
    public Seaweed(int age, int pv, Aquarium aquarium) {
        super(age, pv);
        super.ID = i++;
        this.aquarium = aquarium;
    }
    //GETTER & SETTER
    /**
     * Permet de récupérer l'ID d'une Seaweed
     * @return id (String)
     */
    public int getId() {
        return super.ID;
    }

    /**
     * permet d'ajouter/ de remplacer une ID à une algue instanciée
     * @param id Nouvelle ID
     */
    public void setId(int id) {
        super.ID = id;
    }


    /**
     * méthode permettant à un poisson de se faire manger
     * @param f Fish qui à manger l'autre poisson
     */
    public void attacked(Fish f) {
        System.out.println("L'algue " + f.getID() + " est attaquée par le poisson " + f.getName() + " (elle perd 2 PV)");
        this.setPV(this.getPV() - 2);
        f.setPV(f.getPV() + 3);

        if (this.getPV() <= 0) {
            this.kill();
        }

    }


    /**
     * Permet de tuer un poisson
     */
    public void kill() {
        System.out.println("L'algue " + super.ID + " est morte");
        aquarium.removeSeaweed(this);
    }

    @Override
    public void reproduce() {
        if (this.getPV() > 10) {
            Seaweed s = new Seaweed(0, 5, aquarium);
            aquarium.addSeaweed(s);
            System.out.println("L'algue " + this.getId() + " à donnée naissance à l'algue " + s.getId());
            this.setPV(this.getPV() / 2);
        }
    }
}
