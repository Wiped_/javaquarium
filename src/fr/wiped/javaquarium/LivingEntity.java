package fr.wiped.javaquarium;

public abstract class LivingEntity {

    protected int ID;
    private int age;
    private int PV;

    public LivingEntity(int age, int PV) {
        this.age = age;
        this.PV = PV;

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPV() {
        return PV;
    }

    public void setPV(int PV) {
        this.PV = PV;
    }

    public abstract void reproduce();}
