package fr.wiped.javaquarium;

import fr.wiped.javaquarium.Fish;
import fr.wiped.javaquarium.Seaweed;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Apprendre le code avec Helliot
 */
public class Aquarium {

    private List<Fish> fishInAquarium = new CopyOnWriteArrayList<>();
    private List<Seaweed> seaweedInAquarium = new CopyOnWriteArrayList<>();

    public Aquarium() {

    }

    public Aquarium(CopyOnWriteArrayList<Fish> list, CopyOnWriteArrayList<Seaweed> list2) {
        this.fishInAquarium = list;
        this.seaweedInAquarium = list2;
    }

    /**
     * Ajouter un poisson à un aquarium
     *
     * @param p Fish à ajouter à l'aquarium
     */
    public void addFish(Fish p) {
        fishInAquarium.add(p);
    }

    /**
     * Ajouter une algue à un aquarium
     *
     * @param a Algue à ajouter à l'aquarium
     */
    public void addSeaweed(Seaweed seaweed) {
        seaweedInAquarium.add(seaweed);
    }

    /**
     * Supprimer un poisson d'un aquarium
     *
     * @param p Fish à supprimer de l'aquarium
     */
    public void removeFish(Fish fish) {
        fishInAquarium.remove(fish);
    }

    /**
     * Supprimer une algue d'un aquarium
     *
     * @param s Seaweed à supprimer de l'aquarium
     */
    public void removeSeaweed(Seaweed seaweed) {
        seaweedInAquarium.remove(seaweed);
    }

    /**
     * Permet de récupérer une ArrayList des poissons présents dans l'aquarium
     *
     * @return une liste de poisson présents dans l'aquarium
     */
    public List<Fish> getFishInAquarium() {
        return fishInAquarium;
    }

    /**
     * Permet de récupérer une ArrayList d'algues présentes dans l'aquarium
     *
     * @return une list d'algues présentes dans l'Aquarium
     */
    public List<Seaweed> getSeaweedInAquarium() {
        return seaweedInAquarium;
    }


}
